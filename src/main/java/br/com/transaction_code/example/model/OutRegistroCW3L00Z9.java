package br.com.transaction_code.example.model;

import java.io.Serializable;

import br.com.transaction_code.annotations.TrancodeField;

public class OutRegistroCW3L00Z9 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4805670106069902388L;

	@TrancodeField(pos = 1, length = 2)
	private String LKMAP1_SG_FL_CW3L00Z9;

	@TrancodeField(pos = 3, length = 60)
	private String LKMAP1_NM_LOCL_CW3L00Z9;

	@TrancodeField(pos = 63, length = 10)
	private String LKMAP1_CD_IBGE_CW3L00Z9;

	@TrancodeField(pos = 73, length = 1)
	private String LKMAP1_CD_TP_LOCL_CW3L00Z9;

	@TrancodeField(pos = 74, length = 60)
	private String LKMAP1_NM_LOCOL_LOWC9_CW3L00Z9;

	@TrancodeField(pos = 134, length = 60)
	private String LKMAP1_NM_MUNI_CW3L00Z9;

	@TrancodeField(pos = 194, length = 60)
	private String LKMAP1_NM_MUNI_LOWC_CW3L00Z9;

	public String getLKMAP1_SG_FL_CW3L00Z9() {
		return LKMAP1_SG_FL_CW3L00Z9;
	}

	public void setLKMAP1_SG_FL_CW3L00Z9(String lKMAP1_SG_FL_CW3L00Z9) {
		LKMAP1_SG_FL_CW3L00Z9 = lKMAP1_SG_FL_CW3L00Z9;
	}

	public String getLKMAP1_NM_LOCL_CW3L00Z9() {
		return LKMAP1_NM_LOCL_CW3L00Z9;
	}

	public void setLKMAP1_NM_LOCL_CW3L00Z9(String lKMAP1_NM_LOCL_CW3L00Z9) {
		LKMAP1_NM_LOCL_CW3L00Z9 = lKMAP1_NM_LOCL_CW3L00Z9;
	}

	public String getLKMAP1_CD_IBGE_CW3L00Z9() {
		return LKMAP1_CD_IBGE_CW3L00Z9;
	}

	public void setLKMAP1_CD_IBGE_CW3L00Z9(String lKMAP1_CD_IBGE_CW3L00Z9) {
		LKMAP1_CD_IBGE_CW3L00Z9 = lKMAP1_CD_IBGE_CW3L00Z9;
	}

	public String getLKMAP1_CD_TP_LOCL_CW3L00Z9() {
		return LKMAP1_CD_TP_LOCL_CW3L00Z9;
	}

	public void setLKMAP1_CD_TP_LOCL_CW3L00Z9(String lKMAP1_CD_TP_LOCL_CW3L00Z9) {
		LKMAP1_CD_TP_LOCL_CW3L00Z9 = lKMAP1_CD_TP_LOCL_CW3L00Z9;
	}

	public String getLKMAP1_NM_LOCOL_LOWC9_CW3L00Z9() {
		return LKMAP1_NM_LOCOL_LOWC9_CW3L00Z9;
	}

	public void setLKMAP1_NM_LOCOL_LOWC9_CW3L00Z9(String lKMAP1_NM_LOCOL_LOWC9_CW3L00Z9) {
		LKMAP1_NM_LOCOL_LOWC9_CW3L00Z9 = lKMAP1_NM_LOCOL_LOWC9_CW3L00Z9;
	}

	public String getLKMAP1_NM_MUNI_CW3L00Z9() {
		return LKMAP1_NM_MUNI_CW3L00Z9;
	}

	public void setLKMAP1_NM_MUNI_CW3L00Z9(String lKMAP1_NM_MUNI_CW3L00Z9) {
		LKMAP1_NM_MUNI_CW3L00Z9 = lKMAP1_NM_MUNI_CW3L00Z9;
	}

	public String getLKMAP1_NM_MUNI_LOWC_CW3L00Z9() {
		return LKMAP1_NM_MUNI_LOWC_CW3L00Z9;
	}

	public void setLKMAP1_NM_MUNI_LOWC_CW3L00Z9(String lKMAP1_NM_MUNI_LOWC_CW3L00Z9) {
		LKMAP1_NM_MUNI_LOWC_CW3L00Z9 = lKMAP1_NM_MUNI_LOWC_CW3L00Z9;
	}

}
