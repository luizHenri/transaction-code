package br.com.transaction_code.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import br.com.transaction_code.enumeration.TypeConvert;

@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface TrancodeField {

	/**
	 * Position of the data in the input record, must start from 1 (mandatory).
	 */
	int pos();

	/**
	 * Name of the field (optional)
	 */
	String name() default "";

	/**
	 * Length of the data block if the record is set to a fixed length
	 */
	int length() default 0;

	
	/**
	 * <p>
	 * 	<b>ZERO_TO_LEFT</b>
	 * 	<br>
	 * 	This option is used for to complete the 
	 * 	variable with zero to left;
	 * </p>
	 * 
	 * <p>
	 * <b>ZERO_TO_RIGHT</b>
	 * <br>
	 * This option is used for to complete the 
	 * variable with zero to right;
	 * </p>
	 * 
	 * <p>
	 * 	<b>SPACE_TO_LEFT</b>
	 * 	<br>
	 * 	This option is used for to complete the 
	 * 	variable with space to left;
	 * </p>
	 * 
	 * <p>
	 * 	<b>SPACE_TO_RIGHT</b>
	 *	<br>
	 * 	This option is used for to complete the 
	 * 	variable with space to right;
	 * </p>
	 */
	TypeConvert typeConvert() default TypeConvert.NOTHING;

}
