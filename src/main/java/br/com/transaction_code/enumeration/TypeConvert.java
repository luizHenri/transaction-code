package br.com.transaction_code.enumeration;

public enum TypeConvert {

	ZERO_TO_LEFT, 
	ZERO_TO_RIGHT, 
	SPACE_TO_LEFT, 
	SPACE_TO_RIGHT,
	NOTHING;

}
