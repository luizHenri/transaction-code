package br.com.transaction_code.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface TrancodeFieldArray {

	/**
	 * Position of the data in the input record, must start from 1 (mandatory).
	 */
	int pos();

	/**
	 * Name of the field (optional)
	 */
	String name() default "";

	/**
	 * Length of the data block if the record is set to a fixed length
	 */
	int maxLength();
	
	int arrayLength();

	/**
	 * Identifies a data field in the record that defines the expected fixed
	 * length for this field
	 */
	int lengthPos() default 0;
	
	Class<?> classInArray();
	
	int occurs();

}
