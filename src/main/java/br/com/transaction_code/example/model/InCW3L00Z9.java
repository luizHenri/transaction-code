package br.com.transaction_code.example.model;

import br.com.transaction_code.annotations.TrancodeField;
import br.com.transaction_code.enumeration.TypeConvert;

public class InCW3L00Z9 {


	@TrancodeField(pos = 1, length = 1, typeConvert = TypeConvert.SPACE_TO_RIGHT)
	public String ENT_TP_CONS_CW3L00Z9;

	@TrancodeField(pos = 2, length = 2, typeConvert = TypeConvert.SPACE_TO_RIGHT)
	public String ENT_SG_UF_CW3L00Z9;

	@TrancodeField(pos = 4, length = 1, typeConvert = TypeConvert.SPACE_TO_RIGHT)
	public String ENT_FL_PAG_CW3L00Z9;

	@TrancodeField(pos = 5, length = 10, typeConvert = TypeConvert.SPACE_TO_RIGHT)
	public String ENT_CD_ULT_IBGE_CW3L00Z9;

	public String getENT_TP_CONS_CW3L00Z9() {
		return ENT_TP_CONS_CW3L00Z9;
	}

	public void setENT_TP_CONS_CW3L00Z9(String eNT_TP_CONS_CW3L00Z9) {
		ENT_TP_CONS_CW3L00Z9 = eNT_TP_CONS_CW3L00Z9;
	}

	public String getENT_SG_UF_CW3L00Z9() {
		return ENT_SG_UF_CW3L00Z9;
	}

	public void setENT_SG_UF_CW3L00Z9(String eNT_SG_UF_CW3L00Z9) {
		ENT_SG_UF_CW3L00Z9 = eNT_SG_UF_CW3L00Z9;
	}

	public String getENT_FL_PAG_CW3L00Z9() {
		return ENT_FL_PAG_CW3L00Z9;
	}

	public void setENT_FL_PAG_CW3L00Z9(String eNT_FL_PAG_CW3L00Z9) {
		ENT_FL_PAG_CW3L00Z9 = eNT_FL_PAG_CW3L00Z9;
	}

	public String getENT_CD_ULT_IBGE_CW3L00Z9() {
		return ENT_CD_ULT_IBGE_CW3L00Z9;
	}

	public void setENT_CD_ULT_IBGE_CW3L00Z9(String eNT_CD_ULT_IBGE_CW3L00Z9) {
		ENT_CD_ULT_IBGE_CW3L00Z9 = eNT_CD_ULT_IBGE_CW3L00Z9;
	}
}
