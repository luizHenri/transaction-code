package br.com.transaction_code.engine;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.transaction_code.annotations.TrancodeField;
import br.com.transaction_code.annotations.TrancodeFieldArray;
import br.com.transaction_code.exception.TrancodeException;

public class TransactionCode {

	private Class<?> thisClass;
	private TrancodeField dataField;
	private TrancodeFieldArray dataFieldArray;

	public TransactionCode() {
	}

	public String toTrancode(Object object) throws TrancodeException {

		StringBuffer sb = new StringBuffer();

		try {

			this.thisClass = Class.forName(object.getClass().getName());

			Field[] declaredFileds = thisClass.getDeclaredFields();
			TrancodeField dataField = null;

			for (Field field : declaredFileds) {

				Annotation[] annotation = field.getAnnotations();

				for (Annotation aNN : annotation) {

					dataField = (TrancodeField) aNN;

					if (!field.getType().isAssignableFrom(List.class)) {
						sb.append(appendToString(object, dataField, field));
					} else {

						Class<?> thisClassInArray = Class.forName(dataFieldArray.classInArray().getName());

						Field[] declaredFiledsInArray = thisClassInArray.getDeclaredFields();

						StringBuffer _Array = new StringBuffer();
						String _$ = "";

						Object oInArray;

						for (int y = 0; y < dataFieldArray.occurs(); y++) {

							for (Field _sField : declaredFiledsInArray) {

								_sField.setAccessible(true);

								oInArray = _sField.get(object);

								Annotation[] _sAnnotation = _sField.getAnnotations();

								for (Annotation _aNN : _sAnnotation) {

									this.dataField = (TrancodeField) _aNN;
									_Array.append(appendToString(oInArray, dataField, _sField));

								}

							}

						}

						if (_Array.toString().length() > dataFieldArray.maxLength()) {
							_$ = _Array.toString().substring(0, dataFieldArray.maxLength());
						} else if (_Array.toString().length() < dataFieldArray.maxLength()) {
							_$ = StringUtils.rightPad(_Array.toString(), dataFieldArray.maxLength(), " ");
						}

						sb.append(_$);
					}

				}

			}

		} catch (Exception e) {
			throw new TrancodeException(e);
		}

		return sb.toString();
	}

	public Object fromTrancode(String _String, Class<?> _Class) throws TrancodeException {

		Object object = null;

		try {

			this.thisClass = Class.forName(_Class.getName());

			object = _Class.newInstance();

			Field[] declaredFields = thisClass.getDeclaredFields();

			int index = 0;

			for (Field field : declaredFields) {

				field.setAccessible(true);

				Annotation[] annotation = field.getAnnotations();

				for (Annotation aNN : annotation) {

					index = instanceOF(index, aNN);

					typeOf(field, _String, index, object);

				}

			}

		} catch (Exception e) {
			throw new TrancodeException(e);
		}

		return object;
	}

	private int instanceOF(int index, Annotation a) {
		if (a instanceof TrancodeField) {
			this.dataField = (TrancodeField) a;
			index = this.dataField.pos() - 1;
		} else if (a instanceof TrancodeFieldArray) {
			this.dataFieldArray = (TrancodeFieldArray) a;
		}
		return index;
	}

	private void typeOf(final Field field, final String val, int index, final Object o)
			throws IllegalArgumentException, IllegalAccessException, ClassNotFoundException, InstantiationException {

		if (field.getType().isAssignableFrom(String.class)) {
			field.set(o, val.substring(index, dataField.length() + index).trim());
		} else if (field.getType().isAssignableFrom(Long.class)) {
			field.set(o, Long.parseLong(val.substring(index, dataField.length() + index).trim()));
		} else if (field.getType().isAssignableFrom(Integer.class)) {
			field.set(o, Integer.parseInt(val.substring(index, dataField.length() + index).trim()));
		} else if (field.getType().isAssignableFrom(BigDecimal.class)) {
			field.set(o, new BigDecimal(val.substring(index, dataField.length() + index).trim()));
		} else if (field.getType().isAssignableFrom(Double.class)) {
			field.set(o, Double.parseDouble(val.substring(index, dataField.length() + index).trim()));
		} else if (field.getType().isAssignableFrom(Float.class)) {
			field.set(o, Float.parseFloat(val.substring(index, dataField.length() + index).trim()));
		} else if (field.getType().isAssignableFrom(List.class)) {

			Class<?> thisClassInArray = Class.forName(dataFieldArray.classInArray().getName());

			Field[] declaredFiledsInArray = thisClassInArray.getDeclaredFields();

			String stringInArray = val.substring(dataFieldArray.pos() - 1, dataFieldArray.maxLength());

			int _first = 0;
			int _second = dataFieldArray.arrayLength();

			@SuppressWarnings("unchecked")
			List<Object> oList = (List<Object>) createListOfType(thisClassInArray);

			String _Array = "";
			Object oInArray;

			for (int y = 0; y < dataFieldArray.occurs(); y++) {

				_Array = stringInArray.substring(_first, _second);

				oInArray = thisClassInArray.newInstance();

				for (Field _sField : declaredFiledsInArray) {

					_sField.setAccessible(true);

					Annotation[] _sAnnotation = _sField.getAnnotations();

					for (Annotation _aNN : _sAnnotation) {

						this.dataField = (TrancodeField) _aNN;

						index = dataField.pos() - 1;

						typeOf(_sField, _Array, index, oInArray);
					}

				}

				oList.add(oInArray);

				_first += dataFieldArray.arrayLength();
				_second += dataFieldArray.arrayLength();
			}

			field.set(o, oList);
		}

	}

	private String appendToString(Object object, TrancodeField dataField, Field f) throws IllegalAccessException {

		treatingNULL(f, object);
		
		StringBuffer sb = new StringBuffer();

		switch (dataField.typeConvert()) {

		case ZERO_TO_LEFT:
			sb.append(StringUtils.leftPad(f.get(object).toString(), dataField.length(), "0"));
			break;
		case ZERO_TO_RIGHT:
			sb.append(StringUtils.rightPad(f.get(object).toString(), dataField.length(), "0"));
			break;
		case SPACE_TO_LEFT:
			sb.append(StringUtils.leftPad(f.get(object).toString(), dataField.length(), " "));
			break;
		case SPACE_TO_RIGHT:
			sb.append(StringUtils.rightPad(f.get(object).toString(), dataField.length(), " "));
			break;
		default:
			sb.append(f.get(object).toString());
			break;
		}

		return sb.toString();
	}

	private void treatingNULL(Field field, Object o) throws IllegalArgumentException, IllegalAccessException {

		if (field.get(o) == null) {

			if (field.getType().isAssignableFrom(String.class)) {
				field.set(o, "");
			} else if (field.getType().isAssignableFrom(Long.class)) {
				field.set(o, 0L);
			} else if (field.getType().isAssignableFrom(Integer.class)) {
				field.set(o, 0);
			} else if (field.getType().isAssignableFrom(BigDecimal.class)) {
				field.set(o, new BigDecimal(0));
			} else if (field.getType().isAssignableFrom(Double.class)) {
				field.set(o, 0D);
			} else if (field.getType().isAssignableFrom(Float.class)) {
				field.set(o, 0F);
			}

		}

	}

	private <T> List<T> createListOfType(Class<T> type) {
		return new ArrayList<T>();
	}

}
