package br.com.transaction_code.example.model;

import java.io.Serializable;
import java.util.List;

import br.com.transaction_code.annotations.TrancodeField;
import br.com.transaction_code.annotations.TrancodeFieldArray;

public class OutCW3L00Z9 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4805670106069902388L;

	
	@TrancodeField(pos = 1, length = 3)
	private String CODRET_WSAI;
	
	@TrancodeField(pos = 4, length = 50)
	private String MENSAGEM_WSAI;

	@TrancodeField(pos = 54, length = 1)
	private String LKMAP1_FLAG_CW3L00Z9;

	@TrancodeField(pos = 55, length = 4)
	private String LKMAP1_QT_OCCORR_CW3L00Z9;

	@TrancodeFieldArray(pos = 59, maxLength = 26000, arrayLength = 253, occurs = 100, classInArray = OutRegistroCW3L00Z9.class)
	private List<OutRegistroCW3L00Z9> LKMAP1_REGISTRO_CW3L00Z9;

	public String getLKMAP1_FLAG_CW3L00Z9() {
		return LKMAP1_FLAG_CW3L00Z9;
	}

	public void setLKMAP1_FLAG_CW3L00Z9(String lKMAP1_FLAG_CW3L00Z9) {
		LKMAP1_FLAG_CW3L00Z9 = lKMAP1_FLAG_CW3L00Z9;
	}

	public String getLKMAP1_QT_OCCORR_CW3L00Z9() {
		return LKMAP1_QT_OCCORR_CW3L00Z9;
	}

	public void setLKMAP1_QT_OCCORR_CW3L00Z9(String lKMAP1_QT_OCCORR_CW3L00Z9) {
		LKMAP1_QT_OCCORR_CW3L00Z9 = lKMAP1_QT_OCCORR_CW3L00Z9;
	}

	public List<OutRegistroCW3L00Z9> getLKMAP1_REGISTRO_CW3L00Z9() {
		return LKMAP1_REGISTRO_CW3L00Z9;
	}

	public void setLKMAP1_REGISTRO_CW3L00Z9(List<OutRegistroCW3L00Z9> lKMAP1_REGISTRO_CW3L00Z9) {
		LKMAP1_REGISTRO_CW3L00Z9 = lKMAP1_REGISTRO_CW3L00Z9;
	}

}
