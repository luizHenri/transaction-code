package br.com.transaction_code.exception;

public class TrancodeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 674779818822190192L;

	public TrancodeException() {
	}

	public TrancodeException(String message) {
		super(message);
	}

	public TrancodeException(Throwable cause) {
		super(cause);
	}

	public TrancodeException(String message, Throwable cause) {
		super(message, cause);
	}

	public TrancodeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
